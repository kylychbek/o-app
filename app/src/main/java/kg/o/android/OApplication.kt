package kg.o.android

import android.app.Application
import kg.o.android.repo.AlbumsRepository
import kg.o.android.repo.CommentsRepository
import kg.o.android.repo.PhotoRepository
import kg.o.android.repo.PostsRepository
import kg.o.android.ui.albums.AlbumsViewModel
import kg.o.android.ui.albums.photo.PhotosViewModel
import kg.o.android.ui.post.PostListViewModel
import kg.o.android.ui.post.comments.CommentsViewModel
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.android.startKoin
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import java.util.concurrent.TimeUnit

class OApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, modules())
    }

    private fun modules(): List<Module> {
        return listOf(applicationContext {
            bean { initClient() }

            factory { Request.Builder() }
            factory { AlbumsRepository(get(), get()) }
            factory { PhotoRepository(get(), get()) }
            factory { PostsRepository(get(), get()) }
            factory { CommentsRepository(get(), get()) }

            viewModel { AlbumsViewModel(get()) }
            viewModel { PostListViewModel(get()) }
            viewModel { params -> PhotosViewModel(params["albumId"], get()) }
            viewModel { params -> CommentsViewModel(params["postId"], get()) }
        })
    }

    private fun initClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.HEADERS
            builder.addInterceptor(interceptor)
        }
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.connectTimeout(20, TimeUnit.SECONDS)
        return builder.build()
    }
}