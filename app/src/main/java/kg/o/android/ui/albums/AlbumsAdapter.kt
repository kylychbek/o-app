package kg.o.android.ui.albums

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kg.o.android.R
import kg.o.android.model.Album

class AlbumsAdapter(private val cb: (Album) -> Unit) : ListAdapter<Album, AlbumsAdapter.AlbumsViewHolder>(diffUtils) {
    companion object {
        val diffUtils = object : DiffUtil.ItemCallback<Album>() {
            override fun areItemsTheSame(oldItem: Album?, newItem: Album?) = oldItem?.id == newItem?.id
            override fun areContentsTheSame(oldItem: Album?, newItem: Album?) = oldItem == newItem
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_album, parent, false)
        val vh = AlbumsViewHolder(view)
        vh.itemView.setOnClickListener {
            val pos = vh.adapterPosition
            if (pos != -1) cb(getItem(pos))
        }
        return vh
    }

    override fun onBindViewHolder(holder: AlbumsViewHolder, position: Int) {
        holder.title.text = getItem(position).title
    }


    class AlbumsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.findViewById<TextView>(R.id.title)
    }
}