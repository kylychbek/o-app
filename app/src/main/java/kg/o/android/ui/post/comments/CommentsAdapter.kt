package kg.o.android.ui.post.comments

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kg.o.android.R
import kg.o.android.model.Comment

class CommentsAdapter : ListAdapter<Comment, CommentsAdapter.CommentViewHolder>(diffUtils) {
    companion object {
        val diffUtils = object : DiffUtil.ItemCallback<Comment>() {
            override fun areItemsTheSame(oldItem: Comment?, newItem: Comment?) = oldItem?.id == newItem?.id
            override fun areContentsTheSame(oldItem: Comment?, newItem: Comment?) = oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder(LayoutInflater
                .from(parent.context).inflate(R.layout.item_comment, parent, false))
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.name.text = getItem(position).name
        holder.desc.text = getItem(position).body
        holder.email.text = getItem(position).email
    }


    class CommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.findViewById<TextView>(R.id.name)!!
        val desc = view.findViewById<TextView>(R.id.description)!!
        val email = view.findViewById<TextView>(R.id.email)!!
    }
}