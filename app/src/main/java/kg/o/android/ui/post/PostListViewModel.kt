package kg.o.android.ui.post

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kg.o.android.repo.PostsRepository
import kotlinx.coroutines.experimental.launch

class PostListViewModel(private val repo: PostsRepository) : ViewModel() {

    private val _posts: MutableLiveData<List<Any>> = MutableLiveData()
    val posts: LiveData<List<Any>> get() = _posts

    private val _errors: MutableLiveData<String> = MutableLiveData()
    val errors: LiveData<String> get() = _errors

    init {
        launch {
            val result = repo.getPosts()
            val weathers = repo.getWeathers()
            if (result.exception == null && weathers.exception == null) {
                val commonList = mutableListOf<Any>()
                commonList.addAll(weathers.data!!)
                commonList.addAll(result.data!!)
                commonList.shuffle()
                _posts.postValue(commonList)
            } else if (result.exception != null) {
                _errors.postValue(result.exception.message)
            } else {
                _errors.postValue(weathers.exception!!.message)
            }
        }
    }
}