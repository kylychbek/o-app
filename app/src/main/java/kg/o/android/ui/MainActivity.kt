package kg.o.android.ui

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kg.o.android.R
import kg.o.android.ui.albums.AlbumsFragment
import kg.o.android.ui.post.PostListFragment

class MainActivity : AppCompatActivity() {

    private val fragments: MutableMap<Int, Fragment> = mutableMapOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navigation = findViewById<BottomNavigationView>(R.id.navigation)

        fragments[R.id.tab_posts] = PostListFragment()
        fragments[R.id.tab_albums] = AlbumsFragment()

        navigation.setOnNavigationItemSelectedListener {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.root, fragments[it.itemId])
                    .commit()
            return@setOnNavigationItemSelectedListener true
        }

        navigation.selectedItemId = R.id.tab_posts
    }
}
