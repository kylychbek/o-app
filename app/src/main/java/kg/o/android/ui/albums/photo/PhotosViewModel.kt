package kg.o.android.ui.albums.photo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kg.o.android.model.Photo
import kg.o.android.repo.PhotoRepository
import kotlinx.coroutines.experimental.launch

class PhotosViewModel(private val albumId: Int, private val repository: PhotoRepository) : ViewModel() {

    private val _photos: MutableLiveData<List<Photo>> = MutableLiveData()
    val photos: LiveData<List<Photo>> get() = _photos

    private val _errors: MutableLiveData<String> = MutableLiveData()
    val errors: LiveData<String> get() = _errors

    init {
        launch {
            val result = repository.getPhotos(albumId)
            if (result.exception == null) {
                _photos.postValue(result.data)
            } else {
                _errors.postValue(result.exception.message)
            }
        }
    }
}