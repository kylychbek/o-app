package kg.o.android.ui.albums

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kg.o.android.model.Album
import kg.o.android.repo.AlbumsRepository
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

class AlbumsViewModel(private val repository: AlbumsRepository) : ViewModel() {

    private val _albums: MutableLiveData<List<Album>> = MutableLiveData()
    val albums: LiveData<List<Album>> get() = _albums

    private val _errors: MutableLiveData<String> = MutableLiveData()
    val errors: LiveData<String> get() = _errors

    init {
        launch {
            val result = repository.getAlbums()
            if (result.exception == null) {
                _albums.postValue(result.data)
            } else {
                _errors.postValue(result.exception.message)
            }
        }
    }
}