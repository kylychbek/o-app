package kg.o.android.ui.albums.photo

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import kg.o.android.model.Photo

class PhotosAdapter(private val cb: (Photo) -> Unit) : ListAdapter<Photo, PhotosAdapter.PhotoViewHolder>(diffUtils) {

    companion object {
        val diffUtils = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(oldItem: Photo?, newItem: Photo?) = oldItem?.id == newItem?.id
            override fun areContentsTheSame(oldItem: Photo?, newItem: Photo?) = oldItem == newItem
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = ImageView(parent.context)
        val vh = PhotoViewHolder(view)
        vh.itemView.setOnClickListener {
            val pos = vh.adapterPosition
            if (pos != -1) cb(getItem(pos))
        }
        return vh
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        Glide
                .with(holder.itemView.context)
                .load(getItem(position).thumbnailUrl)
                .into(holder.image)
    }


    class PhotoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view as ImageView
    }
}