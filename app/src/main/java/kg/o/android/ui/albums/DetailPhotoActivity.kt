package kg.o.android.ui.albums

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.bumptech.glide.Glide
import kg.o.android.R

class DetailPhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val textView = findViewById<TextView>(R.id.title)

        textView.text = intent.getStringExtra("title")
        Glide
                .with(this)
                .load(intent.getStringExtra("url"))
                .into(findViewById(R.id.imageView))

    }
}