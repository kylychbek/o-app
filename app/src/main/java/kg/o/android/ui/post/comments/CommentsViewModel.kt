package kg.o.android.ui.post.comments

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kg.o.android.model.Comment
import kg.o.android.repo.CommentsRepository
import kotlinx.coroutines.experimental.launch

class CommentsViewModel(private val postId: Int,
                        private val repo: CommentsRepository) : ViewModel() {

    private val _comments: MutableLiveData<List<Comment>> = MutableLiveData()
    val comments: LiveData<List<Comment>> get() = _comments

    private val _errors: MutableLiveData<String> = MutableLiveData()
    val errors: LiveData<String> get() = _errors

    init {
        launch {
            val result = repo.getComments(postId)
            if (result.exception == null) {
                _comments.postValue(result.data)
            } else {
                _errors.postValue(result.exception.message)
            }
        }
    }
}