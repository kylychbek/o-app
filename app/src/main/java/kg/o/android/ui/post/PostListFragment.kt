package kg.o.android.ui.post

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kg.o.android.R
import kg.o.android.model.Post
import kg.o.android.ui.WebViewActivity
import kg.o.android.ui.albums.photo.PhotosActivity
import kg.o.android.ui.post.comments.CommentsActivity
import org.koin.android.architecture.ext.viewModel

class PostListFragment : Fragment() {

    private val viewModel: PostListViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, b: Bundle?): View? {
        return inflater.inflate(R.layout.list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        val adapter = PostsAdapter(this::onPostClick)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter

        viewModel.posts.observe(this, Observer(adapter::submitList))

        viewModel.errors.observe(this, Observer {
            Toast.makeText(activity!!.applicationContext, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun onPostClick(element: Any) {
        if (element is Post) {
            val intent = Intent(activity, CommentsActivity::class.java)
            intent.putExtra("postId", element.id)
            startActivity(intent)
        } else startActivity(Intent(activity, WebViewActivity::class.java))
    }
}