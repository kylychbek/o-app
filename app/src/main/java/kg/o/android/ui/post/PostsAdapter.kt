package kg.o.android.ui.post

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kg.o.android.R
import kg.o.android.model.Post
import kg.o.android.model.Weather

class PostsAdapter(private val cb: (Any) -> Unit) : RecyclerView.Adapter<ViewHolder>() {

    private var items = listOf<Any>()
    private val POST = 1
    private val WEATHER = 10

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        return if (items[position] is Post) POST else WEATHER
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh: ViewHolder = when (viewType) {
            POST -> {
                PostViewHolder(LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.item_post, parent, false))
            }
            else -> {
                WeatherViewHolder(LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.item_weather, parent, false))
            }
        }
        vh.itemView.setOnClickListener {
            val pos = vh.adapterPosition
            if (pos != -1) cb(items[pos])
        }
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        when (holder) {
            is PostViewHolder -> {
                val item = items[position] as Post
                holder.desc.text = item.body
                holder.title.text = item.title
            }

            is WeatherViewHolder -> {
                val item = items[position] as Weather
                holder.desc.text = item.description
                holder.name.text = item.name
                holder.temp.text = item.temp.toString()
            }
        }
    }

    fun submitList(list: List<Any>?) {
        items = list!!
        notifyDataSetChanged()
    }
}


sealed class ViewHolder(view: View) : RecyclerView.ViewHolder(view)


class PostViewHolder(view: View) : ViewHolder(view) {
    val title = view.findViewById<TextView>(R.id.title)
    val desc = view.findViewById<TextView>(R.id.description)
}

class WeatherViewHolder(view: View) : ViewHolder(view) {
    val name = view.findViewById<TextView>(R.id.city)
    val temp = view.findViewById<TextView>(R.id.temp)
    val desc = view.findViewById<TextView>(R.id.description)
}
