package kg.o.android.ui.albums.photo

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import kg.o.android.R
import kg.o.android.model.Photo
import kg.o.android.ui.albums.DetailPhotoActivity
import org.koin.android.ext.android.inject

class PhotosActivity : AppCompatActivity() {

    private lateinit var viewModel: PhotosViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list)
        val albumId = intent.getIntExtra("albumId", 1)
        viewModel = inject<PhotosViewModel> { mapOf("albumId" to albumId) }.value

        val recyclerView = findViewById<RecyclerView>(R.id.list)
        val adapter = PhotosAdapter(::onPhotoClick)
        recyclerView.layoutManager = GridLayoutManager(this, 3)
        recyclerView.adapter = adapter

        viewModel.photos.observe(this, Observer(adapter::submitList))

        viewModel.errors.observe(this, Observer {
            Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun onPhotoClick(photo: Photo) {
        val intent = Intent(this, DetailPhotoActivity::class.java)
        intent.putExtra("title", photo.title)
        intent.putExtra("url", photo.url)
        startActivity(intent)
    }
}