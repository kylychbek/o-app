package kg.o.android.ui.post.comments

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import kg.o.android.R
import org.koin.android.ext.android.inject

class CommentsActivity : AppCompatActivity() {

    private lateinit var viewModel: CommentsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list)
        val albumId = intent.getIntExtra("postId", 0)
        viewModel = inject<CommentsViewModel> { mapOf("postId" to albumId) }.value


        val recyclerView = findViewById<RecyclerView>(R.id.list)
        val adapter = CommentsAdapter()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        viewModel.comments.observe(this, Observer(adapter::submitList))

        viewModel.errors.observe(this, Observer {
            Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show()
        })
    }
}