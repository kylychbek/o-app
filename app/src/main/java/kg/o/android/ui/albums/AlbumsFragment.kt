package kg.o.android.ui.albums

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kg.o.android.R
import kg.o.android.model.Album
import kg.o.android.ui.albums.photo.PhotosActivity
import org.koin.android.architecture.ext.viewModel

class AlbumsFragment : Fragment() {

    private val viewModel: AlbumsViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, b: Bundle?): View? {
        return inflater.inflate(R.layout.list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        val adapter = AlbumsAdapter(::onAlbumClick)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter

        viewModel.albums.observe(this, Observer(adapter::submitList))

        viewModel.errors.observe(this, Observer {
            Toast.makeText(activity!!.applicationContext, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun onAlbumClick(album: Album) {
        val intent = Intent(activity, PhotosActivity::class.java)
        intent.putExtra("albumId", album.id)
        startActivity(intent)
    }
}