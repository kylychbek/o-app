package kg.o.android.model

data class Photo(val id: Int, val title: String, val url: String, val thumbnailUrl: String)