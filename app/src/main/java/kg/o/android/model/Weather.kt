package kg.o.android.model

data class Weather(val id:Int, val name: String, val temp:Number, val description:String)