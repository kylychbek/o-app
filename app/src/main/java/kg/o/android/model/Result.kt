package kg.o.android.model

data class Result<T>(val data: T? = null, val exception: Exception? = null)