package kg.o.android.model

data class Album(val id: Int, val title: String)
