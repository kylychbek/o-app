package kg.o.android.repo

import kg.o.android.model.Photo
import kg.o.android.model.Result
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import java.lang.RuntimeException

class PhotoRepository(private val client: OkHttpClient,
                      private val builder: Request.Builder) {

    fun getPhotos(albumId: Int): Result<List<Photo>> {
        try {
            val link = "http://jsonplaceholder.typicode.com/photos?albumId=$albumId"
            val response = client.newCall(builder.url(link).build()).execute()

            val body = response.body()?.string() ?: return Result(exception = RuntimeException())
            if (!response.isSuccessful || body.isNullOrEmpty()) return Result(exception = RuntimeException())
            val jsonObject = JSONArray(body)

            val list = mutableListOf<Photo>()

            for (i in 0 until jsonObject.length()) {
                val element = jsonObject[i] as? JSONObject ?: continue
                val id = element["id"] as Int
                val title = element["title"] as? String ?: ""
                val url = element["url"] as? String ?: ""
                val thumbnail = element["thumbnailUrl"] as? String ?: ""
                list.add(Photo(id, title, url, thumbnail))
            }

            return Result(data = list)
        } catch (e: Exception) {
            e.printStackTrace()
            return Result(exception = e)
        }
    }
}