package kg.o.android.repo

import kg.o.android.model.Post
import kg.o.android.model.Result
import kg.o.android.model.Weather
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import java.lang.RuntimeException
import java.util.*

const val WEATHER_URL = "http://api.openweathermap.org/data/2.5/group?id=1528675,1527534,1527592,1528512&units=metric&APPID=c315ea17291e8dfea8875f1f8f4046b4"

class PostsRepository(private val client: OkHttpClient,
                      private val builder: Request.Builder) {

    fun getPosts(): Result<List<Post>> {
        try {
            val request = builder.url("http://jsonplaceholder.typicode.com/posts").build()
            val response = client.newCall(request).execute()

            val body = response.body()?.string() ?: return Result(exception = RuntimeException())
            if (!response.isSuccessful || body.isNullOrEmpty()) return Result(exception = RuntimeException())
            val jsonObject = JSONArray(body)

            val randomInt = Random().nextInt(100 - 10) + 10
            val list = mutableListOf<Post>()

            for (i in (randomInt - 10)..randomInt) {
                val element = jsonObject[i] as? JSONObject ?: continue
                val id = element["id"] as Int
                val title = element["title"] as? String ?: ""
                val desc = element["body"] as? String ?: ""
                list.add(Post(id, title, desc))
            }

            return Result(data = list)
        } catch (e: Exception) {
            e.printStackTrace()
            return Result(exception = e)
        }
    }

    fun getWeathers(): Result<List<Weather>> {
        try {
            val request = builder.url(WEATHER_URL).build()
            val response = client.newCall(request).execute()

            val body = response.body()?.string() ?: return Result(exception = RuntimeException())
            if (!response.isSuccessful || body.isNullOrEmpty()) return Result(exception = RuntimeException())

            val jsonObject = JSONObject(body)
            if (jsonObject.isNull("list")) {
                return Result(exception = RuntimeException())
            }

            val list = mutableListOf<Weather>()

            val citiesJson = jsonObject.getJSONArray("list")
            for (i in 0 until citiesJson.length()) {
                val element = citiesJson[i] as? JSONObject ?: continue
                val main = element["main"] as? JSONObject ?: continue
                val jsonArray = element["weather"] as? JSONArray ?: continue
                val weather = jsonArray[jsonArray.length() - 1] as? JSONObject ?: continue
                val id = element["id"] as? Int ?: 0
                val cityName = element["name"] as? String ?: "Unknown"
                val desc = weather["description"] as? String ?: "Unknown"
                val temp = main["temp"] as? Number ?: 0
                val dd = Weather(id, cityName, temp, desc)
                list.add(dd)
            }
            return Result(data = list)
        } catch (e: Exception) {
            e.printStackTrace()
            return Result(exception = e)
        }
    }
}