package kg.o.android.repo

import kg.o.android.model.Comment
import kg.o.android.model.Result
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import java.lang.RuntimeException

class CommentsRepository(private val client: OkHttpClient,
                         private val builder: Request.Builder) {

    fun getComments(postId: Int): Result<List<Comment>> {
        try {
            val link = "http://jsonplaceholder.typicode.com/comments?postId=$postId"
            val response = client.newCall(builder.url(link).build()).execute()

            val body = response.body()?.string() ?: return Result(exception = RuntimeException())
            if (!response.isSuccessful || body.isNullOrEmpty()) return Result(exception = RuntimeException())
            val jsonObject = JSONArray(body)

            val list = mutableListOf<Comment>()

            for (i in 0 until jsonObject.length()) {
                val element = jsonObject[i] as? JSONObject ?: continue
                val id = element["id"] as Int
                val name = element["name"] as? String ?: ""
                val email = element["email"] as? String ?: ""
                val desc = element["body"] as? String ?: ""
                list.add(Comment(id, name, email, desc))
            }

            return Result(data = list)
        } catch (e: Exception) {
            e.printStackTrace()
            return Result(exception = e)
        }
    }
}