package kg.o.android.repo

import kg.o.android.model.Album
import kg.o.android.model.Result
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import java.lang.RuntimeException
import java.util.*

class AlbumsRepository(private val client: OkHttpClient,
                       private val builder: Request.Builder) {

    fun getAlbums(): Result<List<Album>> {
        try {
            val request = builder.url("http://jsonplaceholder.typicode.com/albums").build()
            val response = client.newCall(request).execute()

            val body = response.body()?.string() ?: return Result(exception = RuntimeException())
            if (!response.isSuccessful || body.isNullOrEmpty()) return Result(exception = RuntimeException())
            val jsonObject = JSONArray(body)

            val randomInt = Random().nextInt(100 - 10) + 10
            val list = mutableListOf<Album>()

            for (i in (randomInt - 10)..randomInt) {
                val element = jsonObject[i] as? JSONObject ?: continue
                val id = element["id"] as Int
                val title = element["title"] as? String ?: ""
                list.add(Album(id, title))
            }

            return Result(data = list)
        } catch (e: Exception) {
            e.printStackTrace()
            return Result(exception = e)
        }
    }
}